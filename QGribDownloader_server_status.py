import os
import json

from PyQt5 import uic
from PyQt5.QtWidgets import QDialog, QTableWidgetItem
from PyQt5.QtNetwork import QNetworkRequest
from PyQt5.QtCore import QUrl, QJsonDocument, QSize
from PyQt5.QtGui import QResizeEvent


from qgis.core import QgsNetworkAccessManager, Qgis


STATUS_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'QGribDownloader_server_status.ui'))

class QGribDownloaderServerStatusDialog(QDialog, STATUS_CLASS):
    def __init__(self, iface, parent=None):
        """Constructor"""
        super(QGribDownloaderServerStatusDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.reply_object = None
        self.iface = iface
        self.setupUi(self)

    def updateServerStatus(self):
        try:
            request = QNetworkRequest()
            request.setUrl(QUrl("http://grbsrv.opengribs.org/getstatus2.php"))
            self.reply_object = QgsNetworkAccessManager.instance().get(request)
            self.reply_object.finished.connect(self.asyncManageResponse)
        except:
            self.iface.messageBar().pushMessage("QGribDownloader", "Status Download error: Could not get statuses", level=Qgis.Critical, duration=5)            

    def asyncManageResponse(self):
        string_reply = str(self.reply_object.readAll(),'utf-8')
        status = json.loads(string_reply)
        self.populate_summary_table(status)
        self.resize_table()

    def populate_summary_table(self, status_dict):
        horizontal_headers = [key[0] for key in status_dict.items()]
        veritcal_headers = [key for key,value in [value for key,value in status_dict.items()][0].items()]
        content = []
        for i in range(len(status_dict)):
            content.append([value for key, value in [value for key,value in status_dict.items()][i].items()])
        self.statusTableWidget.setRowCount(len(veritcal_headers))
        self.statusTableWidget.setColumnCount(len(horizontal_headers))
        self.statusTableWidget.setHorizontalHeaderLabels(horizontal_headers)
        self.statusTableWidget.setVerticalHeaderLabels(veritcal_headers)
        self.format_table_content(content)

    def format_table_content(self, content):
        try:
            for column_index in range(len(content)):
                for row_index in range(len(content[column_index])):
                    table_item = QTableWidgetItem(str(content[column_index][row_index]), 0)
                    self.statusTableWidget.setItem(row_index, column_index, table_item)
        except:
            self.iface.messageBar().pushMessage("QGribDownloader", "Status Parse error: Could not get all statuses", level=Qgis.Critical, duration=5)
            pass

    def resize_table(self):
        column_sizes = 0
        row_sizes = 0
        for i in range(self.statusTableWidget.columnCount()):
            column_size = self.statusTableWidget.sizeHintForColumn(i)
            column_sizes += column_size
        for i in range(self.statusTableWidget.rowCount()):
            row_size = self.statusTableWidget.sizeHintForRow(i)
            row_sizes += row_size
        self.statusTableWidget.horizontalHeader().setStretchLastSection(True)
        self.statusTableWidget.verticalHeader().setStretchLastSection(True)
        old_col_size = QSize()
        new_col_size = QSize()
        old_col_size.setWidth(self.statusTableWidget.width())
        new_col_size.setWidth(column_sizes)
        resize_event = QResizeEvent(new_col_size, old_col_size)
        self.statusTableWidget.resizeEvent(resize_event)
        old_row_size = QSize()
        new_row_size = QSize()
        old_row_size.setWidth(self.statusTableWidget.width())
        new_row_size.setWidth(row_sizes)
        resize_event = QResizeEvent(new_row_size, old_row_size)
        self.statusTableWidget.resizeEvent(resize_event)